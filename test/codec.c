
#include <glib.h>
#include "sflib.h"
#include <json-glib/json-glib.h>

GHashTable *
create_authentication (void)
{
  GHashTable *authentication = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (authentication, (gchar *)"mk", (gchar *)"1d4bf49e62145ce303743e3ae63fc3e48be5b643678a14966a812f9708a1573c");
  g_hash_table_insert (authentication, (gchar *)"ak", (gchar *)"0275bf200689a00a316b90a23feebb798f4d305590bbd9bbd788b0320ef90f0e");

  return authentication;
}

void
test_encrypt_item (void)
{
  g_autofree gchar *json = NULL;
  g_autoptr(JsonNode) node = NULL;
  g_autoptr(GHashTable) authentication = NULL;
  g_autoptr(SfCodec) codec = NULL;
  g_autoptr(SfItem) item = NULL;

  codec = sf_codec_new ();
  authentication = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (authentication, (gchar *)"mk", (gchar *)"aa008fdd1b3680161dfd52ac0eea728da1273a9a4e93c03cc5e074ce76db00a8");
  g_hash_table_insert (authentication, (gchar *)"ak", (gchar *)"0b8e795d22b77e34d155cb1d0c3d891403e58334f4aea2444033c3afc06af961");
  item = sf_item_new_with_uuid ("18241eba-13d1-474b-a52b-344ce4c6b6c8");

  sf_item_set_text (item, "Hello world");
  sf_item_set_title (item, "Foobar");
  sf_item_set_updated_at (item, g_date_time_new_local (2019, 03, 20, 11, 12, 13));
  sf_item_set_created_at (item, g_date_time_new_local (2019, 03, 20, 11, 12, 13));

  node = sf_codec_encrypt (codec, item, authentication);
  json = json_to_string (node, FALSE);
  g_assert_cmpstr (json, ==, "{\"uuid\":\"18241eba-13d1-474b-a52b-344ce4c6b6c8\",\"content_type\":\"Note\",\"enc_item_key\":\"003:f11e1f16ba42b04565dc8813069557df915aaa2db98eac0de78c1759612ec66b:18241eba-13d1-474b-a52b-344ce4c6b6c8:5d3c275742091c69115c14640a4f1a21:Ay3T9fCfQ8LyUYOOTgVrlxHmcvpyZmMU8vZjjssdzVhlmtmhvTxSfjCPj+S/5tKPk4TDm6QBxDblnQmyp4hA9s4GHTZYomwOxI1YWMQEcP+vP3ddYRLrkTW1gtwfnVYpHwYt7ix3IuK4sQcTZx6mdLOb+UAXSbcZmYYPyGpxMQBU1Y2CYGKaGPisRdGiUV5v\",\"content\":\"003:9e87c4309a6bd5df54c04cda168f1f082aaa262fac082ff4c22397253b8ea2b6:18241eba-13d1-474b-a52b-344ce4c6b6c8:17160e1f2c6f694e507019687c27772f:H3pyMZlgXPEU69WENKFA5OuWVi2Wq5j6Fqcb1Pv+1NgORrRbeswdhqzltzRtD/OX0XibuMV6+W0Ct3x0igJiyNsGwbwfV8c/zwtLvxosRz73HsSgSivPjzW+uSssf/Vn\",\"deleted\":false,\"updated_at\":\"2019-03-20T11:12:13Z\",\"created_at\":\"2019-03-20T11:12:13Z\"}");
}

static void
test_decrypt_item (void)
{
  g_autofree gchar *datafile = NULL;
  g_autofree gchar *updated_at = NULL;
  g_autoptr(SfCodec) codec = NULL;
  g_autoptr(GHashTable) authentication = NULL;
  g_autoptr(JsonParser) parser = NULL;
  g_autoptr(SfItem) item = NULL;
  JsonNode *root = NULL;

  codec = sf_codec_new ();
  authentication = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (authentication, (gchar *)"mk", (gchar *)"aa008fdd1b3680161dfd52ac0eea728da1273a9a4e93c03cc5e074ce76db00a8");
  g_hash_table_insert (authentication, (gchar *)"ak", (gchar *)"0b8e795d22b77e34d155cb1d0c3d891403e58334f4aea2444033c3afc06af961");

  datafile = g_test_build_filename (G_TEST_DIST, "data", "item.json", NULL);
  parser = json_parser_new ();

  json_parser_load_from_file (parser, datafile, NULL);
  root = json_parser_get_root (parser);

  item = sf_codec_decrypt (codec, root, authentication);
  g_assert_cmpstr (sf_item_get_title (item), ==, "This is a title");
  g_assert_cmpstr (sf_item_get_text  (item), ==, "This is a text");
  g_assert_cmpstr (sf_item_get_uuid  (item), ==, "3162fe3a-1b5b-4cf5-b88a-afcb9996b23a");
  g_assert_cmpstr (sf_item_get_content_type  (item), ==, "Note");
  updated_at = sf_item_get_updated_at (item);
  g_assert_cmpstr (updated_at, ==, "2016-12-16T17:37:50Z");
}


SfItem *
create_new_item (void)
{
  SfItem *new_item = sf_item_new_with_uuid ("3162fe3a-1b5b-4cf5-b88a-afcb9996b23a");
  sf_item_set_title (new_item, "This is a title");
  sf_item_set_text  (new_item, "This is a text");
  sf_item_set_dirty (new_item, TRUE);
  sf_item_set_updated_at (new_item, g_date_time_new_now_local ());
  sf_item_set_created_at (new_item, g_date_time_new_now_local ());

  return new_item;
}

void
test_roundtrip (void)
{
  g_autoptr(GHashTable) authentication = NULL;
  g_autoptr(SfItem) item = NULL;
  g_autoptr(SfCodec) codec = NULL;
  g_autoptr(JsonNode) item_node = NULL;
  g_autoptr(SfItem) enc_item = NULL;

  // Initialization
  authentication = create_authentication ();

  // Encryption
  codec = sf_codec_new ();
  item = create_new_item ();
  item_node = sf_codec_encrypt (codec, item, authentication);

  // Decryption
  enc_item = sf_codec_decrypt (codec, item_node, authentication);

  g_assert_cmpstr ("This is a title", ==, sf_item_get_title (enc_item));
  g_assert_cmpstr ("This is a text", ==, sf_item_get_text (enc_item));
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/codec/encrypt/item", test_encrypt_item);
  g_test_add_func ("/codec/decrypt/item", test_decrypt_item);
  g_test_add_func ("/codec/roundtrip", test_roundtrip);

  return g_test_run ();
}
