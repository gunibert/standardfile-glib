#include <glib.h>
#include "sf-util.h"
#include <openssl/sha.h>

void
test_substring (void)
{
  const char *input = "Hello, world!";
  char *substr = sf_util_substring (input, 0, 5);
  g_assert_cmpstr (substr, ==, "Hello");
  g_free (substr);

  substr = sf_util_substring (input, 7, 5);
  g_assert_cmpstr (substr, ==, "world");
  g_free (substr);

  substr = sf_util_substring (input, 7, 10);
  g_assert_null (substr);

  substr = sf_util_substring (NULL, 0, 2);
  g_assert_null (substr);

  substr = sf_util_substring (input, -1, 3);
  g_assert_null (substr);

  substr = sf_util_substring (input, 5, -1);
  g_assert_null (substr);
}

void
test_bin_to_hex (void)
{
  struct {
    guchar bin[2];
    const gchar *hex;
  } testdata[] = {
    {.bin = "\x01", .hex = "01"},
    {.bin = "\x02", .hex = "02"},
    {.bin = "\x03", .hex = "03"},
    {.bin = "\x04", .hex = "04"},
    {.bin = "\x05", .hex = "05"},
  };
  for (int i = 0; i < 5; i++) {
    char *hex = sf_util_bin_to_hex (testdata[i].bin, 1);
    g_assert_cmpstr (hex, ==, testdata[i].hex);
    g_free (hex);
  }
}

static void
test_hex_to_bin (void)
{
  struct {
    guchar *bin;
    const char *hex;
  } testdata[] = {
    {.bin = (guchar *) "\x1", .hex = "01"},
    {.bin = (guchar *) "\x2", .hex = "02"},
    {.bin = (guchar *) "\x3", .hex = "03"},
    {.bin = (guchar *) "\x4", .hex = "04"},
    {.bin = (guchar *) "\x5", .hex = "05"},
  };
  for (int i = 0; i < 5; i++) {
    unsigned char *bin = sf_util_hex_to_bin (testdata[i].hex);
    g_assert_cmpmem (bin, 1, testdata[i].bin, 1);
    g_free (bin);
  }

}

static void
test_b64decode (void)
{
  size_t len;
  const char *b64str = "SGVsbG8sIHdvcmxkIQ==";
  guchar *result = g_base64_decode (b64str, &len);
  g_assert_cmpstr ((char *)result, ==, "Hello, world!");
  g_assert_cmpint (len, ==, 13);
  g_assert_cmpint (strlen ((char *)result), ==, 13);
}

static void
test_b64encode (void)
{
  guchar *data = (guchar *) "Hello, world!";
  char *result = g_base64_encode (data, 13);
  g_assert_cmpstr (result, ==, "SGVsbG8sIHdvcmxkIQ==");
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/util/substring", test_substring);
  g_test_add_func ("/util/bin_to_hex", test_bin_to_hex);
  g_test_add_func ("/util/hex_to_bin", test_hex_to_bin);
  g_test_add_func ("/util/b64decode", test_b64decode);
  g_test_add_func ("/util/b64encode", test_b64encode);

  return g_test_run ();
}
