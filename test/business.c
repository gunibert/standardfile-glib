
#include <glib.h>
#include "sflib.h"
#include "mocks/test-utils.h"

gboolean has_authorization = FALSE;
gboolean no_pw_cost = FALSE;

void
test_register (gconstpointer user_data)
{
  GError *error = NULL;
  g_autofree gchar *address = NULL;
  g_autoptr(SfClient) client = NULL;
  const SoupURI *uri = user_data;

  address = g_strdup_printf ("http://localhost:%d", soup_uri_get_port ((SoupURI *)uri));
  client = sf_client_new (address);

  sf_client_register (client, "info@gunibert.de", "test", &error);

  g_assert_null (error);
}

void
test_authentication (gconstpointer user_data)
{
  GError *error = NULL;
  g_autofree char *address;
  g_autoptr(SfClient) client;
  const SoupURI *uri = user_data;
  has_authorization = FALSE;

  address = g_strdup_printf ("http://localhost:%d", soup_uri_get_port ((SoupURI *)uri));
  client = sf_client_new (address);
  if (!sf_client_authenticate (client, "info@gunibert.de", "test", &error)) {
    g_test_fail ();
  }

  sf_client_sync_items (client, NULL);

  g_assert_null (error);
  g_assert_true (has_authorization);
}

GMainLoop *loop = NULL;

static void
test_auth_async_cb (GObject      *source_object,
                    GAsyncResult *res,
                    gpointer      user_data)
{
  SfClient *client = NULL;
  GError *error = NULL;
  has_authorization = FALSE;

  client = SF_CLIENT (source_object);

  sf_client_authenticate_finish (client, res, &error);

  sf_client_sync_items (client, NULL);

  g_assert_null (error);
  g_assert_true (has_authorization);
  g_main_loop_quit (loop);
}

void
test_authentication_async (gconstpointer user_data)
{
  g_autoptr(SfClient) client = NULL;
  g_autofree char *address;
  const SoupURI *uri = user_data;

  loop = g_main_loop_new (NULL, FALSE);
  address = g_strdup_printf ("http://localhost:%d", soup_uri_get_port ((SoupURI *)uri));
  client = sf_client_new (address);
  sf_client_authenticate_async (client, "info@gunibert.de", "test", NULL, test_auth_async_cb, NULL);

  g_main_loop_run (loop);
}

void
test_synchronization (gconstpointer user_data)
{
    const SoupURI *uri = user_data;
    GError *error = NULL;
    GPtrArray *items = NULL;
    g_autoptr(SfClient) client;
    g_autofree char *address;

    address = g_strdup_printf ("http://localhost:%d", soup_uri_get_port ((SoupURI *)uri));
    client = sf_client_new (address);

    items = sf_client_get_items (client);
    g_assert_cmpint (items->len, ==, 0);

    if (!sf_client_authenticate (client, "info@gunibert.de", "test", &error)) {
        g_test_fail ();
    }

    sf_client_sync_items (client, NULL);
    g_assert_null (error);

    items = sf_client_get_items (client);
    g_assert_cmpint (items->len, ==, 1);
}

void
test_auth_no_pw_cost (gconstpointer user_data)
{
    g_autofree char *address;
    g_autoptr(SfClient) client = NULL;
    GError *error = NULL;
    gboolean success = FALSE;
    const SoupURI *uri = NULL;
    no_pw_cost = TRUE;

    uri = user_data;

    address = g_strdup_printf ("http://localhost:%d", soup_uri_get_port ((SoupURI *)uri));
    client = sf_client_new (address);
    success = sf_client_authenticate (client, "info@gunibert.de", "test", &error);

    g_assert_false (success);

    no_pw_cost = FALSE;
}

static void
server_handler (SoupServer        *server,
                SoupMessage       *msg,
                const char        *path,
                GHashTable        *query,
                SoupClientContext *client,
                gpointer           user_data)
{
    if (g_strcmp0 (path, "/auth/params") == 0)
        {
            const gchar *payload;
            if (no_pw_cost)
                payload = "{\"pw_nonce\":\"abc\", \"version\":\"003\"}\n";
            else
                payload = "{\"pw_cost\":110000, \"pw_nonce\":\"292815aeb0b23bb3c6060ba1fb13a3d37f738350d7f00cd26d0b3e2bc2df9354\", \"version\":\"003\"}\n";
            soup_message_set_response (msg, "application/json", SOUP_MEMORY_STATIC, payload, strlen (payload));
            soup_message_set_status (msg, SOUP_STATUS_OK);
            return;
        }
    else if (g_strcmp0 (path, "/auth/sign_in") == 0 || g_strcmp0 (path, "/auth") == 0)
        {
            const gchar *payload = "{\"token\":\"hallowelt\"}\n";
            soup_message_set_response (msg, "application/json", SOUP_MEMORY_STATIC, payload, strlen (payload));
            soup_message_set_status (msg, SOUP_STATUS_OK);
            return;
        }
    else if (g_strcmp0 (path, "/items/sync") == 0)
        {
            g_autofree gchar *datafile;
            gchar *content;
            gsize contentlen;
            const char *name, *value;
            SoupMessageHeadersIter iter;
            SoupMessageHeaders *headers = msg->request_headers;

            datafile = g_test_build_filename (G_TEST_DIST, "data", "items.json", NULL);
            g_file_get_contents (datafile, &content, &contentlen, NULL);

            soup_message_headers_iter_init (&iter, headers);
            while (soup_message_headers_iter_next (&iter, &name, &value))
                {
                    if (g_strcmp0 (name, "Authorization") == 0 && g_strcmp0 (value, "Bearer ") > 0)
                        has_authorization = TRUE;
                }

            soup_message_set_response (msg, "application/json", SOUP_MEMORY_TAKE, content, contentlen);
            soup_message_set_status (msg, 200);

            return;
        }
}

gint
main (gint   argc,
      gchar *argv[])
{
  SoupURI *uri;
  SoupServer *server;
  int ret;

  g_test_init (&argc, &argv, NULL);

  server = soup_test_server_new (TRUE);
  soup_server_add_handler (server, NULL, server_handler, NULL, NULL);
  uri = soup_test_server_get_uri (server, "http", NULL);

  g_test_add_data_func ("/business/register", uri, test_register);
  g_test_add_data_func ("/business/auth", uri, test_authentication);
  g_test_add_data_func ("/business/auth_async", uri, test_authentication_async);
  g_test_add_data_func ("/business/auth_no_pw_cost", uri, test_auth_no_pw_cost);
  g_test_add_data_func ("/business/synchronization", uri, test_synchronization);

  ret = g_test_run ();

  soup_uri_free (uri);
  soup_test_server_quit_unref (server);

  g_main_context_unref (g_main_context_default ());

  return ret;
  }
