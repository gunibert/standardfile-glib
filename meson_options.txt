option('plantuml', type: 'boolean', value: 'true')
option('tracing', type: 'boolean', value: 'true')
option('gtk_doc', type: 'feature', value: 'disabled', yield: true)
option('introspection',
       type: 'feature', value: 'auto', yield: true,
       description: 'Build the introspection data (requires gobject-introspection)')
