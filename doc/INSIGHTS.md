# AES encryption

I had problems to replicate the encryption algorithm because i've never worked
with AES encryption or OpenSSL. Therefore i had to do several experiments to
get the correct input parameters in a fitting way.

## Binary data vs. Hex data

The OpenSSL library takes the initialization vector (IV) and the key as binary
data. The string to encrypt doesn't need to be binary.
