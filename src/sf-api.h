/* sf-api.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

#define SF_TYPE_API (sf_api_get_type())

G_DECLARE_DERIVABLE_TYPE (SfApi, sf_api, SF, API, GObject)

/**
 * SfApiClass:
 *
 * The `SfApiClass` structure contains only private data and should be accessed
 * using the provided API
 *
 * Since: 0.2
 */
struct _SfApiClass
{
  /*< private >*/
  GObjectClass parent_class;
};

/**
 * SfApiRoute:
 * @SF_API_AUTH: register an account and get an JWT
 * @SF_API_AUTH_CHANGE_PW: change password of existing account
 * @SF_API_AUTH_SIGN_IN: sign into an existing account
 * @SF_API_AUTH_PARAMS: get generation parameters of associated account
 * @SF_API_ITEMS_SYNC: sync items
 *
 * This enum defines several routing endpoints
 */
typedef enum {
  SF_API_AUTH,
  SF_API_AUTH_CHANGE_PW,
  SF_API_AUTH_SIGN_IN,
  SF_API_AUTH_PARAMS,
  SF_API_ITEMS_SYNC
} SfApiRoute;

SfApi       *sf_api_new     (char        *host);
const gchar *sf_api_get_jwt (SfApi       *self);
void         sf_api_set_jwt (SfApi       *self,
                             const gchar *jwt);
SoupMessage *sf_api_get     (SfApi       *self,
                             SfApiRoute   route);
SoupMessage *sf_api_post    (SfApi       *self,
                             SfApiRoute   route);

G_END_DECLS
