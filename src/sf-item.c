/* sf-item.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:sf-item
 * @Title: SfItem
 * @short_description: the model object of a Standardfile Note
 *
 * The #SfItem is the model object of a Standardfile Note. Besides of that
 * every object managed by Standardfile is a #SfItem (like Tags or Preferences)
 */

#define G_LOG_DOMAIN "sf-item"

#include "sf-item.h"
#include "sf-util.h"

typedef struct
{
  char *uuid;

  SfContent *content;
  char *content_type;
  GDateTime *created_at;
  GDateTime *updated_at;
  gboolean deleted;

  /*< private >*/
  gboolean dirty;
} SfItemPrivate;

G_DEFINE_TYPE_WITH_CODE (SfItem, sf_item, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (SfItem)
                         )

enum {
  PROP_0,
  PROP_UUID,
  PROP_TITLE,
  PROP_TEXT,
  PROP_PREVIEW_PLAIN,
  PROP_CONTENT_TYPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * sf_item_new:
 *
 * Creates a new #SfItem object and automatically associates an UUID with it.
 *
 * Returns: a #SfItem
 *
 * Since: 0.2
 */
SfItem *
sf_item_new (void)
{
  g_autofree char *uuid = NULL;

  uuid = g_uuid_string_random ();
  return g_object_new (SF_TYPE_ITEM,
                       "uuid", uuid,
                       "content-type", "Note",
                       NULL);
}

/**
 * sf_item_new_with_uuid:
 * @uuid: a uuid string
 *
 * Creates a new #SfItem object with a specific UUID. This is used to recreate
 * model objects from the server.
 *
 * Returns:a #SfItem
 *
 * Since: 0.2
 */
SfItem *
sf_item_new_with_uuid (const char *uuid)
{
  return g_object_new (SF_TYPE_ITEM,
                       "uuid", uuid,
                       "content-type", "Note",
                       NULL);
}

static void
sf_item_finalize (GObject *object)
{
  SfItem *self = (SfItem *)object;
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_clear_pointer (&priv->uuid, g_free);
  g_clear_object (&priv->content);
  g_clear_pointer (&priv->content_type, g_free);
  g_clear_pointer (&priv->created_at, g_date_time_unref);
  g_clear_pointer (&priv->updated_at, g_date_time_unref);

  G_OBJECT_CLASS (sf_item_parent_class)->finalize (object);
}

static void
sf_item_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  SfItem *self = SF_ITEM (object);
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_UUID:
      g_value_set_string (value, priv->uuid);
      break;
    case PROP_TITLE:
      g_value_set_string (value, sf_content_get_title (priv->content));
      break;
    case PROP_TEXT:
      g_value_set_string (value, sf_content_get_text (priv->content));
      break;
    case PROP_PREVIEW_PLAIN:
      g_value_set_string (value, sf_content_get_preview_plain (priv->content));
      break;
    case PROP_CONTENT_TYPE:
      g_value_set_string (value, priv->content_type);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sf_item_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  SfItem *self = SF_ITEM (object);
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_UUID:
      g_clear_pointer (&priv->uuid, g_free);
      priv->uuid = g_value_dup_string (value);
      break;
    case PROP_TITLE:
      sf_content_set_title (priv->content, g_value_get_string (value));
      break;
    case PROP_TEXT:
      sf_content_set_text (priv->content, g_value_get_string (value));
      g_object_set (self, "preview-plain", g_value_get_string (value), NULL);
      break;
    case PROP_PREVIEW_PLAIN:
      sf_content_set_preview_plain (priv->content, g_value_get_string (value));
      break;
    case PROP_CONTENT_TYPE:
      g_clear_pointer (&priv->content_type, g_free);
      priv->content_type = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sf_item_class_init (SfItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sf_item_finalize;
  object_class->get_property = sf_item_get_property;
  object_class->set_property = sf_item_set_property;

  properties [PROP_UUID] =
    g_param_spec_string ("uuid",
                         "Uuid",
                         "the uuid associated with this item",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "Title",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_TEXT] =
    g_param_spec_string ("text",
                         "Text",
                         "Text",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_PREVIEW_PLAIN] =
    g_param_spec_string ("preview-plain",
                         "PreviewPlain",
                         "PreviewPlain",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_CONTENT_TYPE] =
    g_param_spec_string ("content-type",
                         "ContentType",
                         "The ContentType of that SfItem",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sf_item_init (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);
  priv->content = sf_content_new_empty ();
  priv->dirty = FALSE;
  priv->deleted = FALSE;
}

/**
 * sf_item_get_title:
 * @self: a #SfItem
 *
 * Returns the title of #SfItem.
 *
 * Returns: (transfer none): a title
 *
 * Since: 0.2
 */
const char *
sf_item_get_title (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return sf_content_get_title (priv->content);
}

/**
 * sf_item_set_title:
 * @self: a #SfItem
 * @title: the new title of the item
 *
 * Set the title of #SfItem
 *
 * Since: 0.2
 */
void
sf_item_set_title (SfItem     *self,
                   const char *title)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_if_fail (SF_IS_ITEM (self));
  g_return_if_fail (title != NULL);

  sf_content_set_title (priv->content, title);
}

/**
 * sf_item_get_text:
 * @self: a #SfItem
 *
 * Returns the text of that #SfItem.
 *
 * Returns: (transfer none): the text
 *
 * Since: 0.2
 */
const char *
sf_item_get_text (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return sf_content_get_text (priv->content);
}

/**
 * sf_item_set_text:
 * @self: a #SfItem
 * @text: the new text of the item
 *
 * Set the text of #SfItem
 *
 * Since: 0.2
 */
void
sf_item_set_text (SfItem     *self,
                  const char *text)
{
  g_return_if_fail (SF_IS_ITEM (self));
  g_return_if_fail (text != NULL);

  g_object_set (self, "text", text, NULL);
}

const gchar *
sf_item_get_preview (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return sf_content_get_preview_plain (priv->content);
}

/**
 * sf_item_get_uuid:
 * @self: a #SfItem
 *
 * Returns the uuid of that #SfItem.
 *
 * Returns: (transfer none): the uuid
 *
 * Since: 0.2
 */
const char *
sf_item_get_uuid (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return priv->uuid;
}

/**
 * sf_item_is_note:
 * @self: a #SfItem
 *
 * Checks if #SfItem is a note item.
 *
 * Returns: #TRUE if item is a note, #FALSE otherwise
 *
 * Since: 0.2
 */
gboolean
sf_item_is_note (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), FALSE);

  if (g_strcmp0 (priv->content_type, "Note") == 0)
    return TRUE;

  return FALSE;
}

SfContent *
_sf_item_get_content (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return priv->content;
}

/**
 * sf_item_get_content_type:
 * @self: a #SfItem
 *
 * Returns the Content Type of #SfItem. Could be a Note, Preference or Tag for
 * example.
 *
 * Returns: the content type
 *
 * Since: 0.2
 */
const char *
sf_item_get_content_type (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return priv->content_type;
}

/**
 * sf_item_get_updated_at:
 * @self: a #SfItem
 *
 * Returns the DateTime of the last update of this #SfItem.
 *
 * Returns: (transfer full): a DateTime string
 *
 * Since: 0.2
 */
char *
sf_item_get_updated_at (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return g_date_time_format (priv->updated_at, "%FT%TZ");
}

/**
 * sf_item_get_updated_at_dt:
 *
 * Returns the #GDateTime of the last update of this #SfItem
 *
 * Returns: a #GDateTime
 *
 * Since: 0.2
 */
GDateTime *
sf_item_get_updated_at_dt (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return priv->updated_at;
}

/**
 * sf_item_set_updated_at:
 * @self: a #SfItem
 * @updated_at: (transfer full): a #GDateTime
 *
 * Sets the #GDateTime of the last update of this #SfItem.
 *
 * Since: 0.2
 */
void
sf_item_set_updated_at (SfItem    *self,
                        GDateTime *updated_at)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_if_fail (SF_IS_ITEM (self));

  g_clear_pointer (&priv->updated_at, g_date_time_unref);
  priv->updated_at = updated_at;
}

/**
 * sf_item_set_created_at:
 * @self: a #SfItem
 * @created_at: (transfer full): a #GDateTime
 *
 * Sets the #GDateTime of the creation of this #SfItem.
 *
 * Since: 0.2
 */
void
sf_item_set_created_at (SfItem    *self,
                        GDateTime *created_at)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_if_fail (SF_IS_ITEM (self));

  g_clear_pointer (&priv->created_at, g_date_time_unref);
  priv->created_at = created_at;
}

/**
 * sf_item_get_created_at:
 * @self: a #SfItem
 *
 * Returns the DateTime of the creation of this #SfItem.
 *
 * Returns: (transfer full): a DateTime string
 *
 * Since: 0.2
 */
char *
sf_item_get_created_at (SfItem     *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), NULL);

  return g_date_time_format (priv->created_at, "%FT%TZ");
}

/**
 * sf_item_set_dirty:
 * @self: a #SfItem
 * @dirty: a gboolean
 *
 * Sets this #SfItem dirty. The next sync will transmit the changes of this
 * #SfItem.
 *
 * Since: 0.2
 */
void
sf_item_set_dirty (SfItem   *self,
                   gboolean  dirty)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_if_fail (SF_IS_ITEM (self));

  priv->dirty = dirty;
}

/**
 * sf_item_is_dirty:
 * @self: a #SfItem
 *
 * Returns the internal state of this #SfItem. If its dirty the next sync will
 * transmit this #SfItem.
 *
 * Returns: a gboolean
 *
 * Since: 0.2
 */
gboolean
sf_item_is_dirty (SfItem     *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), FALSE);

  return priv->dirty;
}

/**
 * sf_item_delete:
 * @self: a #SfItem
 * @deleted: a gboolean
 *
 * Marks this #SfItem for deletion.
 *
 * Since: 0.2
 */
void
sf_item_delete (SfItem   *self,
                gboolean  deleted)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_if_fail (SF_IS_ITEM (self));

  priv->deleted = deleted;
}

/**
 * sf_item_is_deleted:
 * @self: a #SfItem
 *
 * Returns #TRUE if this #SfItem is marked for deletion. #FALSE otherwise.
 *
 * Returns: a gboolean
 *
 * Since: 0.2
 */
gboolean
sf_item_is_deleted (SfItem *self)
{
  SfItemPrivate *priv = sf_item_get_instance_private (self);

  g_return_val_if_fail (SF_IS_ITEM (self), FALSE);

  return priv->deleted;
}
