/* sf-content.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "sf-content.h"
#include "sf-debug.h"

struct _SfContent
{
  GObject parent_instance;
  char *text;
  char *title;

  char *preview_plain;
  char *preview_html;
};

G_DEFINE_TYPE (SfContent, sf_content, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_TEXT,
  PROP_TITLE,
  PROP_PREVIEW_PLAIN,
  PROP_PREVIEW_HTML,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

SfContent *
sf_content_new (char *title,
                char *text,
                char *preview_plain,
                char *preview_html)
{
  return g_object_new (SF_TYPE_CONTENT,
                       "title", title,
                       "text", text,
                       "preview-plain", preview_plain,
                       "preview-html", preview_html,
                       NULL);
}

SfContent  *
sf_content_new_empty (void)
{
  return g_object_new (SF_TYPE_CONTENT, NULL);
}

static void
sf_content_finalize (GObject *object)
{
  SfContent *self = (SfContent *)object;

  g_clear_pointer (&self->text, g_free);
  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->preview_plain, g_free);
  g_clear_pointer (&self->preview_html, g_free);

  G_OBJECT_CLASS (sf_content_parent_class)->finalize (object);
}

static void
sf_content_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  SfContent *self = SF_CONTENT (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, sf_content_get_title (self));
      break;
    case PROP_TEXT:
      g_value_set_string (value, sf_content_get_text (self));
      break;
    case PROP_PREVIEW_PLAIN:
      g_value_set_string (value, sf_content_get_preview_plain (self));
      break;
    case PROP_PREVIEW_HTML:
      g_value_set_string (value, sf_content_get_preview_html (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sf_content_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  SfContent *self = SF_CONTENT (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      sf_content_set_title (self, g_value_get_string (value));
      break;
    case PROP_TEXT:
      sf_content_set_text (self, g_value_get_string (value));
      break;
    case PROP_PREVIEW_PLAIN:
      sf_content_set_preview_plain (self, g_value_get_string (value));
      break;
    case PROP_PREVIEW_HTML:
      sf_content_set_preview_html (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sf_content_class_init (SfContentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sf_content_finalize;
  object_class->get_property = sf_content_get_property;
  object_class->set_property = sf_content_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "Title",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_TEXT] =
    g_param_spec_string ("text",
                         "Text",
                         "Text",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_PREVIEW_PLAIN] =
    g_param_spec_string ("preview-plain",
                         "PreviewPlain",
                         "PreviewPlain",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_PREVIEW_HTML] =
    g_param_spec_string ("preview-html",
                         "PreviewHtml",
                         "PreviewHtml",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sf_content_init (_unused SfContent *self)
{
}

const char *
sf_content_get_title (SfContent *self)
{
  g_return_val_if_fail (SF_IS_CONTENT (self), NULL);

  return self->title;
}

void
sf_content_set_title (SfContent  *self,
                      const char *title)
{
  g_return_if_fail (SF_IS_CONTENT (self));
  g_return_if_fail (title != NULL);

  g_clear_pointer (&self->title, g_free);
  self->title = g_strdup (title);
}

const char *
sf_content_get_text (SfContent *self)
{
  g_return_val_if_fail (SF_IS_CONTENT (self), NULL);

  return self->text;
}

void
sf_content_set_text (SfContent  *self,
                     const char *text)
{
  g_return_if_fail (SF_IS_CONTENT (self));
  g_return_if_fail (text != NULL);

  g_clear_pointer (&self->text, g_free);
  self->text = g_strdup (text);
}

const char *
sf_content_get_preview_plain (SfContent *self)
{
  g_return_val_if_fail (SF_IS_CONTENT (self), NULL);

  return self->preview_plain;
}

void
sf_content_set_preview_plain (SfContent   *self,
                              const gchar *preview_plain)
{
  g_return_if_fail (SF_IS_CONTENT (self));
  g_return_if_fail (preview_plain != NULL);

  g_clear_pointer (&self->preview_plain, g_free);
  self->preview_plain = g_strndup (preview_plain, 100);
}

const char *
sf_content_get_preview_html (SfContent *self)
{
  g_return_val_if_fail (SF_IS_CONTENT (self), NULL);

  return self->preview_html;
}

void
sf_content_set_preview_html  (SfContent   *self,
                              const gchar *preview_html)
{
  g_return_if_fail (SF_IS_CONTENT (self));
  g_return_if_fail (preview_html != NULL);

  g_clear_pointer (&self->preview_html, g_free);
  self->preview_html = g_strdup (preview_html);
}
