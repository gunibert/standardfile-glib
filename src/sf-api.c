/* sf-api.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:sf-api
 * @Title: SfApi
 * @short_description: Generates API messages with authentication
 *
 * The #SfApi object creates #SoupMessage with JSON Web token authentication.
 * It defines the endpoints for all API calls of the standardfile protocol.
 */

#define G_LOG_DOMAIN "sf-api"

#include "sf-api.h"
#include "sf-debug.h"

typedef struct
{
  char *host;
  gchar *jwt;
} SfApiPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(SfApi, sf_api, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_HOST,
  PROP_JWT,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static const gchar *
sf_api_get_route (SfApiRoute route)
{
  switch (route)
    {
    case SF_API_AUTH:
      return "auth";
    case SF_API_AUTH_PARAMS:
      return "auth/params";
    case SF_API_AUTH_SIGN_IN:
      return "auth/sign_in";
    case SF_API_AUTH_CHANGE_PW:
      return "auth/change_pw";
    case SF_API_ITEMS_SYNC:
      return "items/sync";
    default:
      return "undefined";
    }
}

static void
sf_api_add_jwt (SfApi       *self,
                SoupMessage *msg)
{
  g_autofree gchar *header = NULL;
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  g_return_if_fail (SF_IS_API (self));
  g_return_if_fail (SOUP_IS_MESSAGE (msg));

  if (priv->jwt != NULL)
    {
      header = g_strconcat ("Bearer ", priv->jwt, NULL);
      soup_message_headers_append(msg->request_headers, "Authorization", header);
    }
}

/**
 * sf_api_new:
 * @host: the hostaddress of the standardserver
 *
 * Creates a new #SfApi object. Every communication with the Standardserver
 * gets handled via this object.
 *
 * Returns: a newly created SfApi object
 *
 * Since: 0.2
 */
SfApi *
sf_api_new (char *host)
{
  return g_object_new (SF_TYPE_API,
                       "host", host,
                       NULL);
}

static void
sf_api_finalize (GObject *object)
{
  SfApi *self = (SfApi *) object;
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  g_clear_pointer (&priv->host, g_free);
  g_clear_pointer (&priv->jwt, g_free);

  G_OBJECT_CLASS (sf_api_parent_class)->finalize (object);
}

static void
sf_api_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  SfApi *self = SF_API (object);
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_HOST:
      g_value_set_string (value, priv->host);
      break;
    case PROP_JWT:
      g_value_set_string (value, priv->jwt);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}

static void
sf_api_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  SfApi *self = SF_API (object);
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_HOST:
      g_clear_pointer(&priv->host, g_free);
      priv->host = g_value_dup_string (value);
      break;
    case PROP_JWT:
      sf_api_set_jwt (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}

static void
sf_api_class_init (SfApiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->finalize = sf_api_finalize;
  object_class->get_property = sf_api_get_property;
  object_class->set_property = sf_api_set_property;

  properties[PROP_HOST] =
    g_param_spec_string ("host",
                         "host",
                         "The host address of the standardfile server",
                         "",
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties[PROP_JWT] =
    g_param_spec_string ("jwt",
                         "jwt",
                         "The JSON web token used for authentication",
                         "",
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sf_api_init (_unused SfApi *self)
{
}

const gchar *
sf_api_get_jwt (SfApi *self)
{
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  g_return_val_if_fail (SF_IS_API (self), NULL);

  return priv->jwt;
}

/**
 * sf_api_set_jwt:
 * @self: a #SfApi
 * @jwt: the JSON Web token
 *
 * Associates the #SfApi object with a JSON Web token. Every #SoupMessage
 * gets this as Authentication header.
 *
 * Since: 0.2
 */
void
sf_api_set_jwt (SfApi       *self,
                const gchar *jwt)
{
  SfApiPrivate *priv = sf_api_get_instance_private (self);

  g_return_if_fail (SF_IS_API (self));
  g_return_if_fail (jwt != NULL);

  g_clear_pointer (&priv->jwt, g_free);
  priv->jwt = g_strdup (jwt);
}

/**
 * sf_api_get:
 * @self: the #SfApi
 * @route: the route endpoint
 *
 * This method initializes a new #SoupMessage to #route with GET as http operation.
 * The authentication header is automatically added.
 *
 * Returns: (transfer full): a #SoupMessage
 *
 * Since: 0.2
 */
SoupMessage *
sf_api_get  (SfApi      *self,
             SfApiRoute  route)
{
  g_autofree gchar *uri = NULL;
  SoupMessage *msg = NULL;
  SfApiPrivate *priv = sf_api_get_instance_private(self);

  g_return_val_if_fail (SF_IS_API (self), NULL);

  uri = g_strjoin ("/", priv->host, sf_api_get_route (route), NULL);
  msg = soup_message_new ("GET", uri);
  sf_api_add_jwt (self, msg);

  return msg;
}

/**
 * sf_api_post:
 * @self: the #SfApi
 * @route: the route endpoint
 *
 * This method initializes a new #SoupMessage to #route with POST as http operation.
 * The authentication header is automatically added.
 *
 * Returns: (transfer full): a #SoupMessage
 *
 * Since: 0.2
 */
SoupMessage *
sf_api_post (SfApi      *self,
             SfApiRoute  route)
{
  g_autofree gchar *uri = NULL;
  SoupMessage *msg = NULL;
  SfApiPrivate *priv = sf_api_get_instance_private(self);

  g_return_val_if_fail (SF_IS_API (self), NULL);

  uri = g_strjoin ("/", priv->host, sf_api_get_route (route), NULL);
  msg = soup_message_new ("POST", uri);
  sf_api_add_jwt (self, msg);

  return msg;
}
