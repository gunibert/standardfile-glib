/* sf-provider.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:sf-provider
 * @Title: SfProvider
 * @short_description: a #GListModel implementation which holds all items
 *
 * The #SfProvider is the datastore for all #SfItem objects. It implements the
 * #GListModel interface for easy consumption by GTK applications.
 */

#define G_LOG_DOMAIN "sf-provider"

#include "sf-provider.h"
#include "sf-debug.h"
#include <gio/gio.h>

struct _SfProvider
{
  GObject parent_instance;

  GPtrArray *items;
};

static void list_model_iface_init (GListModelInterface *iface);
G_DEFINE_TYPE_WITH_CODE (SfProvider, sf_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init)
                         )

SfProvider *
sf_provider_new (void)
{
  return g_object_new (SF_TYPE_PROVIDER, NULL);
}

static void
sf_provider_finalize (GObject *object)
{
  SfProvider *self = (SfProvider *)object;

  g_ptr_array_free (self->items, TRUE);

  G_OBJECT_CLASS (sf_provider_parent_class)->finalize (object);
}

static void
sf_provider_class_init (SfProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sf_provider_finalize;
}

static void
sf_provider_init (SfProvider *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
}

static GType
sf_provider_get_item_type (GListModel *list)
{
  return SF_TYPE_ITEM;
}

static gpointer
sf_provider_get_item (GListModel *list,
                      guint       position)
{
  SfItem *item = NULL;
  SfProvider *self = SF_PROVIDER (list);

  SF_ENTRY;
  g_assert (SF_IS_PROVIDER (self));
  if (position > self->items->len) return NULL;

  item = g_ptr_array_index (self->items, position);
  if (!item)
    return NULL;
  return g_object_ref (item);
}

static guint
sf_provider_get_n_items (GListModel *list)
{
  SfProvider *self = SF_PROVIDER (list);

  g_assert (SF_IS_PROVIDER (self));

  return self->items->len;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = sf_provider_get_item_type;
  iface->get_item = sf_provider_get_item;
  iface->get_n_items = sf_provider_get_n_items;
}

static gboolean
uuid_equal (gconstpointer a,
            gconstpointer b)
{
  SfItem *item_a = (SfItem *)a;
  SfItem *item_b = (SfItem *)b;

  return g_str_equal (sf_item_get_uuid (item_a), sf_item_get_uuid (item_b));
}

static gint
_sort_by_date (gconstpointer item_a,
               gconstpointer item_b)
{
  SfItem **a = (SfItem **) item_a;
  SfItem **b = (SfItem **) item_b;

  return -g_date_time_compare (sf_item_get_updated_at_dt (*a), sf_item_get_updated_at_dt (*b));
}

static void
sf_provider_merge_items (SfProvider *self,
                         SfItem     *original,
                         SfItem     *new)
{
  g_return_if_fail (SF_IS_PROVIDER (self));
  g_return_if_fail (SF_IS_ITEM (original));
  g_return_if_fail (SF_IS_ITEM (new));

  if (g_strcmp0 (sf_item_get_title (original), sf_item_get_title (new)) != 0)
    sf_item_set_title (original, sf_item_get_title (new));

  if (g_strcmp0 (sf_item_get_text (original), sf_item_get_text (new)) != 0)
    sf_item_set_text (original, sf_item_get_text (new));
}

/**
 * sf_provider_add:
 * @self: a #SfProvider
 * @item: a #SfItem
 *
 * Adds a new #SfItem to the model. If item is already exists the data of the
 * new item gets transfered. Otherwise the item will be inserted sorted after
 * the update timestamp.
 *
 * Returns: #TRUE if item is new, #FALSE if item is already in list
 *
 * Since: 0.1
 */
gboolean
sf_provider_add (SfProvider *self,
                 SfItem     *item)
{
  guint pos = 0;

  SF_ENTRY;

  g_return_val_if_fail (SF_IS_PROVIDER (self), FALSE);

  if (g_ptr_array_find_with_equal_func (self->items, item, uuid_equal, &pos))
    {
      SfItem *original = g_ptr_array_index (self->items, pos);
      sf_provider_merge_items (self, original, item);
      g_object_unref (item);
      return FALSE;
    }

  g_ptr_array_add (self->items, item);
  g_ptr_array_sort (self->items, _sort_by_date);
  g_list_model_items_changed (G_LIST_MODEL (self), 0, 0, 1);
  return TRUE;
}

/**
 * sf_provider_add_tag:
 * @self: a #SfProvider
 * @item: a #SfItem
 *
 * Not implemented yet!
 *
 * Since: 0.1
 */
void
sf_provider_add_tag (SfProvider *self,
                     SfItem     *item)
{
  g_return_if_fail (SF_IS_PROVIDER (self));
  g_return_if_fail (SF_IS_ITEM (item));

}

/**
 * sf_provider_remove:
 * @self: a #SfProvider
 * @item: a #SfItem
 *
 * Removes the item from the model. If item is not found does nothing.
 *
 * Returns:
 *
 * Since: 0.1
 */
void
sf_provider_remove (SfProvider *self,
                    SfItem     *item)
{
  gboolean found = FALSE;
  guint pos = 0;

  SF_ENTRY;

  g_return_if_fail (SF_IS_PROVIDER (self));
  g_return_if_fail (SF_IS_ITEM (item));

  found = g_ptr_array_find_with_equal_func (self->items, item, uuid_equal, &pos);
  if (!found)
    return;
  g_list_model_items_changed (G_LIST_MODEL (self), pos, 1, 0);

  SF_PROBE;

  g_ptr_array_remove_index (self->items, pos);
}

/**
 * sf_provider_container:
 *
 * Returns the internal array with all items. Shouldn't be used by library
 * consumers. Exists for API stability reasons.
 *
 * Returns: the pointer array with all items
 *
 * Since: 0.1
 */
GPtrArray *
sf_provider_container (SfProvider *self)
{
  g_return_val_if_fail (SF_IS_PROVIDER (self), NULL);

  return g_ptr_array_ref (self->items);
}
