/* sf-content.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define SF_TYPE_CONTENT (sf_content_get_type())

G_DECLARE_FINAL_TYPE (SfContent, sf_content, SF, CONTENT, GObject)

SfContent  *sf_content_new               (char      *title,
                                          char      *text,
                                          char      *preview_plain,
                                          char      *preview_html);
SfContent  *sf_content_new_empty         (void);
const char *sf_content_get_title         (SfContent *self);
void        sf_content_set_title         (SfContent  *self,
                                          const char *title);
const char *sf_content_get_text          (SfContent *self);
void        sf_content_set_text          (SfContent  *self,
                                          const char *text);
void        sf_content_set_preview_plain (SfContent   *self,
                                          const gchar *preview_plain);
const char *sf_content_get_preview_plain (SfContent *self);
void        sf_content_set_preview_html  (SfContent   *self,
                                          const gchar *preview_html);
const char *sf_content_get_preview_html  (SfContent *self);

G_END_DECLS
