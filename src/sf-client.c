/* sf-client.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:sf-client
 * @Title: SfClient
 * @short_description: the Interface to a standardfile server
 *
 * The #SfClient handles everything to work with a standardfile server. A client
 * should mainly use this to communicate.
 */

#define G_LOG_DOMAIN "sf-client"

#include "sf-client.h"
#include "sf-api.h"
#include "sf-codec.h"
#include "sf-debug.h"
#include "sf-util.h"
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>
#include <openssl/evp.h>

typedef struct
{
  char *host;
  SoupSession *session;

  gchar *hash;
  GHashTable *authentication;

  SfApi *api;
  SfProvider *provider;
  SfCodec *codec;

  gchar *sync_token;
} SfClientPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(SfClient, sf_client, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_HOST,
  PROP_HASH,
  N_PROPS
};

enum
{
  NEW_ITEM,
  NUM_SIGNALS
};

static guint       signals[NUM_SIGNALS] = { 0, };
static GParamSpec *properties[N_PROPS];

/**
 * sf_client_new:
 * @host: the host address of the standardfile server
 *
 * Create a new #SfClient.
 *
 * Returns: (transfer full): a newly created #SfClient
 */
SfClient *
sf_client_new (const gchar *host)
{
  return g_object_new (SF_TYPE_CLIENT,
                       "host", host,
                       NULL);
}

static void
sf_client_constructed (GObject *object)
{
  SfClient *self = SF_CLIENT (object);
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  priv->api = sf_api_new (priv->host);
}

static void
sf_client_finalize (GObject *object)
{
  SfClient *self = (SfClient *) object;
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_clear_pointer (&priv->host, g_free);
  g_clear_object (&priv->session);
  g_clear_object (&priv->api);
  g_clear_object (&priv->provider);
  g_hash_table_unref (priv->authentication);
  g_clear_pointer (&priv->hash, g_free);

  G_OBJECT_CLASS (sf_client_parent_class)->finalize (object);
}

static void
sf_client_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  SfClient *self = SF_CLIENT (object);
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_HOST:
      g_value_set_string (value, priv->host);
      break;
    case PROP_HASH:
      g_value_set_string (value, sf_client_get_hash (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}

static void
sf_client_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  SfClient *self = SF_CLIENT (object);
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_HOST:
      g_clear_pointer(&priv->host, g_free);
      priv->host = g_value_dup_string (value);
      break;
    case PROP_HASH:
      sf_client_set_hash (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}

static void
sf_client_class_init (SfClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->finalize = sf_client_finalize;
  object_class->get_property = sf_client_get_property;
  object_class->set_property = sf_client_set_property;
  object_class->constructed = sf_client_constructed;

  properties [PROP_HOST] =
    g_param_spec_string ("host",
                         "Host",
                         "the host address of the standardfile server",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_HASH] =
    g_param_spec_string ("hash",
                         "Hash",
                         "Hash",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * SfClient::new-item:
   * @client: the client which received the signal
   * @item: the new #SfItem
   *
   * The :new-item signal is emitted every time a new #SfItem is created
   *
   */
  signals [NEW_ITEM] =
    g_signal_new ("new-item",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1,
                  SF_TYPE_ITEM);
}

static void
sf_client_init (SfClient *self)
{
  SfClientPrivate *priv = sf_client_get_instance_private(self);

  priv->authentication = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_free);
  priv->codec = sf_codec_new ();
  priv->session = soup_session_new ();
  priv->provider = sf_provider_new ();
}

static GHashTable *
parse_auth_params (const char *json)
{
  GHashTable *params = NULL;
  g_autoptr (JsonParser) parser = NULL;
  GError *error = NULL;
  JsonNode *root = NULL;
  JsonObject *obj = NULL;

  params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_free);
  if (json == NULL)
    {
      g_hash_table_unref (params);
      return NULL;
    }

  parser = json_parser_new ();
  SF_TRACE ("Json Payload: %s", json);

  if (!json_parser_load_from_data (parser, json, strlen (json), &error)) {
      //FIXME: do something with error
      g_hash_table_unref (params);
      return NULL;
  }

  root = json_parser_get_root (parser);
  obj = json_node_get_object (root);

  if (json_object_has_member (obj, "pw_cost"))
    {
      gint pw_cost  = json_object_get_int_member (obj, "pw_cost");
      g_hash_table_insert (params, (gchar *)"pw_cost", g_strdup_printf ("%d", pw_cost));
    }

  if (json_object_has_member (obj, "pw_nonce"))
    {
      const gchar *pw_nonce = json_object_get_string_member (obj, "pw_nonce");
      g_hash_table_insert (params, (gchar *)"pw_nonce", g_strdup (pw_nonce));
    }

  if (json_object_has_member (obj, "version"))
    {
      const gchar *version  = json_object_get_string_member (obj, "version");
      g_hash_table_insert (params, (gchar *)"version", g_strdup (version));
    }

  if (json_object_has_member (obj, "pw_salt"))
    {
      const gchar *pw_salt = json_object_get_string_member (obj, "pw_salt");
      g_hash_table_insert (params, (gchar *)"pw_salt", g_strdup (pw_salt));
    }

  return params;
}

static const gchar *
parse_authentication (const char *json)
{
  g_autoptr (JsonParser) parser = NULL;
  JsonNode *root = NULL;
  JsonObject *obj = NULL;
  GError *error = NULL;
  const gchar *jwt = NULL;

  g_return_val_if_fail (json != NULL, "");

  parser = json_parser_new ();
  SF_TRACE ("Json Payload: %s", json);

  if (!json_parser_load_from_data (parser, json, strlen (json), &error))
    {
      //FIXME: do something with error
    }

  root = json_parser_get_root (parser);
  obj = json_node_get_object (root);

  jwt = json_object_get_string_member (obj, "token");
  return g_strdup (jwt);
}

static char*
build_salt_keyword (const gchar *email,
                    GHashTable  *params)
{
  char *keyword = g_strjoin (":", email, "SF",
	     g_hash_table_lookup (params, "version"),
	     g_hash_table_lookup (params, "pw_cost"),
	     g_hash_table_lookup (params, "pw_nonce"),
	     NULL
  );

  return keyword;
}

static void
calculate_hash (const char *password, const char *salt, int iterations, unsigned char *out)
{
  PKCS5_PBKDF2_HMAC (password, strlen (password), (const unsigned char *)salt, strlen (salt),
                     iterations, EVP_sha512 (), 768, out);
}

static void
split_hash (SfClient *self,
            const char *hash)
{
  SfClientPrivate *priv = sf_client_get_instance_private(self);
  gint len = 0;

  len = strlen (hash);

  g_hash_table_insert(priv->authentication, (gchar *)"pw", sf_util_substring (hash, 0 * len/3, len/3));
  g_hash_table_insert(priv->authentication, (gchar *)"mk", sf_util_substring (hash, 1 * len/3, len/3));
  g_hash_table_insert(priv->authentication, (gchar *)"ak", sf_util_substring (hash, 2 * len/3, len/3));

  SF_TRACE ("mk: %s", (char *) g_hash_table_lookup (priv->authentication, "mk"));
  SF_TRACE ("ak: %s", (char *) g_hash_table_lookup (priv->authentication, "ak"));
  SF_TRACE ("pw: %s", (char *) g_hash_table_lookup (priv->authentication, "pw"));
}

static GHashTable *
sf_client_fetch_auth_params (SfClient     *self,
                             const gchar  *email,
                             GError      **error)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);
  g_autofree char *url = NULL;
  g_autoptr(SoupURI) uri = NULL;
  g_autoptr(SoupMessage) msg = NULL;
  GHashTable *params = NULL;

  g_return_val_if_fail (SF_IS_CLIENT (self), NULL);

  msg = sf_api_get (priv->api, SF_API_AUTH_PARAMS);

  url = g_strjoin("/", priv->host, "auth", "params", NULL);
  uri = soup_uri_new (url);
  soup_uri_set_query_from_fields (uri, "email", email, NULL);
  soup_message_set_uri (msg, uri);

  soup_session_send_message (priv->session, msg);

  if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) {
    g_set_error (error,
                 soup_http_error_quark (),
                 SOUP_HTTP_ERROR,
                 "Could not connect to server: status %d reason %s",
                 msg->status_code,
                 msg->reason_phrase);
    return NULL;
  }

  params = parse_auth_params (msg->response_body->data);

  return params;
}

/**
 * sf_client_register:
 * @self: a #SfClient
 * @email: the email address
 * @password: the desired password
 * @error: a #GError
 *
 * Registers a new account on the standardfile server
 *
 * Returns: #TRUE if successful, #FALSE otherwise
 *
 * Since: 0.2
 */
gboolean
sf_client_register (SfClient     *self,
                    const gchar  *email,
                    const gchar  *password,
                    GError      **error)
{
  g_autofree gchar *keyword = NULL;
  g_autofree gchar *salt = NULL;
  g_autofree const gchar *jwt = NULL;
  g_autoptr(GHashTable) params = NULL;
  g_autoptr(SoupMessage) msg = NULL;
  g_autoptr(JsonBuilder) builder = NULL;
  g_autoptr(JsonNode) root = NULL;
  gchar *pw_cost_str = NULL;
  gchar *hash_hex = NULL;
  gchar *pw_nonce = NULL;
  gchar *json = NULL;
  gint pw_cost = 0;
  gint status_code = 0;
  guchar hash[768];

  SfClientPrivate *priv = sf_client_get_instance_private (self);
  pw_cost = g_random_int_range (100000, 1000000);
  pw_cost_str = g_strdup_printf ("%d", pw_cost);
  pw_nonce = sf_util_generate_random_string (256);

  params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_free);
  g_hash_table_insert (params, (gchar *)"version", g_strdup ("003"));
  g_hash_table_insert (params, (gchar *)"pw_cost", pw_cost_str);
  g_hash_table_insert (params, (gchar *)"pw_nonce", pw_nonce);

  keyword = build_salt_keyword (email, params);
  salt = g_compute_checksum_for_string (G_CHECKSUM_SHA256, keyword, -1);

  calculate_hash (password, salt, pw_cost, hash);
  hash_hex = sf_util_bin_to_hex (hash, 96);
  priv->hash = hash_hex;
  split_hash (self, hash_hex);

  msg = sf_api_post (priv->api, SF_API_AUTH);

  builder = json_builder_new ();
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "email");
  json_builder_add_string_value (builder, email);
  json_builder_set_member_name (builder, "password");
  json_builder_add_string_value (builder,
                                 g_hash_table_lookup (priv->authentication, "pw"));
  json_builder_set_member_name (builder, "pw_cost");
  json_builder_add_int_value (builder, pw_cost);
  json_builder_set_member_name (builder, "pw_nonce");
  json_builder_add_string_value (builder, pw_nonce);
  json_builder_set_member_name (builder, "version");
  json_builder_add_string_value (builder, "003");
  json_builder_end_object (builder);

  root = json_builder_get_root (builder);
  json = json_to_string (root, FALSE);

  soup_message_set_request (msg, "application/json", SOUP_MEMORY_TAKE, json, strlen(json));
  status_code = soup_session_send_message (priv->session, msg);
  if (!SOUP_STATUS_IS_SUCCESSFUL (status_code))
    {
      g_set_error (error, SOUP_HTTP_ERROR, 0, "Something bad happened...: %s", msg->reason_phrase);
      return FALSE;
    }

  jwt = parse_authentication (msg->response_body->data);

  sf_api_set_jwt (priv->api, jwt);

  return TRUE;
}

/**
 * sf_client_authenticate:
 * @self: a SfClient
 * @email: the email address
 * @password: the users password
 * @error: a #GError
 *
 * Authenticate on remote host and aquire a JWT token.
 *
 * Returns: #TRUE if successful, #FALSE otherwise
 *
 * Since: 0.2
 */
gboolean
sf_client_authenticate (SfClient    *self,
                        const char  *email,
                        const char  *password,
                        GError     **error)
{
  g_autofree gchar *keyword = NULL;
  g_autofree gchar *salt = NULL;
  g_autofree const gchar *jwt = NULL;
  g_autoptr(SoupMessage) msg = NULL;
  g_autoptr(JsonBuilder) builder = NULL;
  g_autoptr(JsonNode) root = NULL;
  const char *pw_cost_str = NULL;
  GHashTable *params = NULL;
  gchar *hash_hex = NULL;
  guchar hash[768];
  gchar *json = NULL;
  int pw_cost = 0;
  int status_code = 0;

  SfClientPrivate *priv = sf_client_get_instance_private (self);
  params = sf_client_fetch_auth_params (self, email, error);
  if (!params)
    return FALSE;

  // salt
  keyword = build_salt_keyword(email, params);
  SF_TRACE ("Keyword %s", keyword);
  salt = g_compute_checksum_for_string (G_CHECKSUM_SHA256, keyword, -1);
  SF_TRACE ("Built salt: %s", salt);

  // hash
  pw_cost_str = g_hash_table_lookup (params, "pw_cost");
  if (pw_cost_str == NULL) {
    SF_TRACE ("%s", "Got no pw_cost informations from server");
    g_hash_table_unref (params);
    return FALSE;
  }

  pw_cost = atoi (pw_cost_str);
  calculate_hash (password, salt, pw_cost, hash);
  hash_hex = sf_util_bin_to_hex (hash, 96);
  SF_TRACE ("Calculated hash: %s", hash_hex);
  g_hash_table_unref (params);

  priv->hash = hash_hex;
  split_hash (self, hash_hex);

  msg = sf_api_post (priv->api, SF_API_AUTH_SIGN_IN);

  builder = json_builder_new ();
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "email");
  json_builder_add_string_value (builder, email);
  json_builder_set_member_name (builder, "password");
  json_builder_add_string_value (builder, g_hash_table_lookup (priv->authentication, "pw"));
  json_builder_end_object (builder);

  root = json_builder_get_root (builder);
  json = json_to_string (root, FALSE);

  soup_message_set_request (msg, "application/json", SOUP_MEMORY_TAKE, json, strlen(json));
  status_code = soup_session_send_message (priv->session, msg);
  if (!SOUP_STATUS_IS_SUCCESSFUL (status_code)) {
    g_set_error (error, SOUP_HTTP_ERROR, 0, "Something bad happened...: %s", msg->reason_phrase);
    return FALSE;
  }

  jwt = parse_authentication(msg->response_body->data);

  sf_api_set_jwt (priv->api, jwt);

  return TRUE;
}

typedef struct {
  const gchar *email;
  const gchar *password;
} Credentials;

static void
sf_client_authenticate_cb (GTask        *task,
                           gpointer      source_object,
                           gpointer      task_data,
                           GCancellable *cancellable)
{
  g_autofree gchar *keyword = NULL;
  g_autofree gchar *salt = NULL;
  g_autofree const gchar *jwt = NULL;
  g_autoptr(GHashTable) params = NULL;
  g_autoptr(SoupMessage) msg = NULL;
  g_autoptr(JsonBuilder) builder = NULL;
  g_autoptr(JsonNode) root = NULL;
  SfClient *self = NULL;
  SfClientPrivate *priv = NULL;
  GError *error = NULL;
  gchar *hash_hex = NULL;
  Credentials *credentials = NULL;
  const gchar *pw_cost_str = NULL;
  gchar *json = NULL;
  gint pw_cost = 0;
  gint status_code = 0;
  guchar hash[768];

  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  credentials = (Credentials *)task_data;
  self = SF_CLIENT (source_object);
  priv = sf_client_get_instance_private (self);
  params = sf_client_fetch_auth_params (self, credentials->email, &error);
  if (!params)
    {
      g_task_return_error (task, error);
      return;
    }

  keyword = build_salt_keyword (credentials->email, params);
  salt = g_compute_checksum_for_string (G_CHECKSUM_SHA256, keyword, -1);

  pw_cost_str = g_hash_table_lookup (params, "pw_cost");
  pw_cost = atoi (pw_cost_str);
  calculate_hash (credentials->password, salt, pw_cost, hash);
  hash_hex = sf_util_bin_to_hex (hash, 96);
  priv->hash = hash_hex;
  split_hash (self, hash_hex);

  msg = sf_api_post (priv->api, SF_API_AUTH_SIGN_IN);

  builder = json_builder_new ();
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "email");
  json_builder_add_string_value (builder, credentials->email);
  json_builder_set_member_name (builder, "password");
  json_builder_add_string_value (builder,
                                 g_hash_table_lookup (priv->authentication, "pw"));
  json_builder_end_object (builder);

  root = json_builder_get_root (builder);
  json = json_to_string (root, FALSE);

  soup_message_set_request (msg, "application/json", SOUP_MEMORY_TAKE, json, strlen(json));
  status_code = soup_session_send_message (priv->session, msg);
  if (!SOUP_STATUS_IS_SUCCESSFUL (status_code)) {
    g_task_return_new_error (task, SOUP_HTTP_ERROR, 0, "Something bad happened...: %s", msg->reason_phrase);
    return;
  }

  jwt = parse_authentication (msg->response_body->data);

  sf_api_set_jwt (priv->api, jwt);

  g_task_return_boolean (task, TRUE);
}

void
sf_client_authenticate_async (SfClient            *self,
                              const gchar         *email,
                              const gchar         *password,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  Credentials *credentials = NULL;

  credentials = g_new0 (Credentials, 1);
  credentials->email = email;
  credentials->password = password;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, credentials, g_free);
  g_task_run_in_thread (task, sf_client_authenticate_cb);
}

/**
 * sf_client_authenticate_finish:
 * @self: a #SfClient
 * @res: a task
 * @error: a #GError
 *
 * Completes an asynchron authentication process.
 *
 * Returns: a gboolean
 *
 * Since: 0.2
 */
gboolean
sf_client_authenticate_finish (SfClient      *self,
                               GAsyncResult  *res,
                               GError       **error)
{
  g_return_val_if_fail (SF_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (res), FALSE);

  return g_task_propagate_boolean (G_TASK (res), error);
}

void
sf_client_set_jwt (SfClient    *self,
                   const gchar *jwt)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);
  g_return_if_fail (SF_IS_CLIENT (self));

  sf_api_set_jwt (priv->api, jwt);
}

const gchar *
sf_client_get_jwt (SfClient *self)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_val_if_fail (SF_IS_CLIENT (self), NULL);
  g_return_val_if_fail (SF_IS_API (priv->api), NULL);

  return sf_api_get_jwt (priv->api);
}

void
sf_client_set_hash (SfClient    *self,
                    const gchar *hash)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_if_fail (SF_IS_CLIENT (self));

  if (g_strcmp0 (priv->hash, hash) != 0)
    {
      g_clear_pointer (&priv->hash, g_free);
      priv->hash = g_strdup (hash);
      split_hash (self, hash);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_HASH]);
    }
}

const gchar *
sf_client_get_hash (SfClient *self)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_val_if_fail (SF_IS_CLIENT (self), NULL);

  return priv->hash;
}

static void
sf_client_merge_or_insert_item (SfClient *self,
                                SfItem   *item)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);
  gboolean new_item = FALSE;

  g_return_if_fail (SF_IS_CLIENT (self));
  g_return_if_fail (SF_IS_ITEM (item));

  if (sf_item_is_note (item))
    {
      new_item = sf_provider_add (priv->provider, item);
      if (new_item)
        g_signal_emit (self, signals[NEW_ITEM], 0, item);
    }
  else
    {
      sf_provider_add_tag (priv->provider, item);
    }
}

static void
sf_client_remove_item (SfClient *self,
                       SfItem   *item)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_if_fail (SF_IS_CLIENT (self));
  g_return_if_fail (SF_IS_ITEM (item));

  sf_provider_remove (priv->provider, item);
}

static JsonNode *
sf_client_prepare_sync_items (SfClient *self)
{
  g_autoptr(JsonBuilder) builder = NULL;
  JsonNode *root = NULL;
  GList *dirty_items = NULL;
  SfClientPrivate *priv = sf_client_get_instance_private (self);
  gint n_items = 0;

  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->provider));
  for (gint i = 0; i < n_items; i++)
    {
      SfItem *item = g_list_model_get_item (G_LIST_MODEL (priv->provider), i);

      if (sf_item_is_dirty (item) || sf_item_is_deleted (item))
        dirty_items = g_list_append (dirty_items, item);
    }

  builder = json_builder_new ();
  json_builder_begin_object (builder);

  json_builder_set_member_name (builder, "items");
  json_builder_begin_array (builder);
  for (GList *cur = dirty_items; cur != NULL; cur = g_list_next (cur)) {
    SfItem *item;
    JsonNode *node;

    item = cur->data;
    node = sf_codec_encrypt (priv->codec, item, priv->authentication);
    json_builder_add_value (builder, node);
  }
  json_builder_end_array (builder);
  json_builder_set_member_name (builder, "sync_token");
  json_builder_add_string_value (builder, priv->sync_token);
  json_builder_end_object (builder);

  root = json_builder_get_root (builder);

  return root;
}

static void
sf_client_sync_items_worker (GTask        *task,
                             gpointer      source_object,
                             gpointer      task_data,
                             GCancellable *cancellable)
{
  g_autoptr(JsonNode) dirty_items = NULL;
  g_autoptr(JsonParser) parser = NULL;
  g_autoptr(SoupMessage) msg = NULL;
  JsonNode *root = NULL;
  JsonObject *root_obj = NULL;
  JsonArray *all_items = NULL;
  JsonArray *saved_items = NULL;
  gchar *json = NULL;
  guint status_code = 0;
  guint len = 0;
  SfClient *self = SF_CLIENT (source_object);
  SfClientPrivate *priv = sf_client_get_instance_private(self);

  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  dirty_items = sf_client_prepare_sync_items (self);
  json = json_to_string (dirty_items, TRUE);
  SF_TRACE ("%s", json);

  msg = sf_api_post (priv->api, SF_API_ITEMS_SYNC);
  soup_message_set_request (msg, "application/json", SOUP_MEMORY_TAKE, json, strlen(json));
  status_code = soup_session_send_message (priv->session, msg);

  if (!SOUP_STATUS_IS_SUCCESSFUL (status_code))
    {
      g_task_return_new_error (task, SOUP_HTTP_ERROR, 0, "Something bad happened...: %s", msg->reason_phrase);
      return;
    }

  parser = json_parser_new();
  json_parser_load_from_data (parser, msg->response_body->data, strlen (msg->response_body->data), NULL);

  root = json_parser_get_root (parser);
  if (root == NULL) return;
  json = json_to_string (root, TRUE);
  SF_TRACE("%s", json);
  root_obj = json_node_get_object (root);
  priv->sync_token = g_strdup (json_object_get_string_member (root_obj, "sync_token"));
  all_items = json_object_get_array_member (root_obj, "retrieved_items");
  len = json_array_get_length (all_items);

  for (uint i = 0; i < len; i++)
    {
      JsonNode *item = NULL;
      SfItem *pitem = NULL;

      item = json_array_get_element (all_items, i);

      pitem = sf_codec_decrypt (priv->codec, item, priv->authentication);
      if (pitem && !sf_item_is_deleted (pitem))
          sf_client_merge_or_insert_item (self, pitem);
      else if (pitem && sf_item_is_deleted (pitem))
          sf_client_remove_item (self, pitem);
    }

  saved_items = json_object_get_array_member (root_obj, "saved_items");
  len = json_array_get_length (saved_items);

  for (uint i = 0; i < len; i++)
    {
      JsonNode *item = NULL;
      SfItem *pitem = NULL;

      item = json_array_get_element (saved_items, i);

      pitem = sf_codec_decrypt (priv->codec, item, priv->authentication);
      if (pitem && !sf_item_is_deleted (pitem))
          sf_client_merge_or_insert_item (self, pitem);
      else if (pitem && sf_item_is_deleted (pitem))
          sf_client_remove_item (self, pitem);
    }

  g_task_return_boolean (task, TRUE);
}

void
sf_client_sync_items (SfClient  *self,
                      GError   **error)
{
  g_autoptr(GTask) task = NULL;

  task = g_task_new (self, NULL, NULL, NULL);
  sf_client_sync_items_worker (task, self, NULL, NULL);
  g_task_propagate_boolean (task, error);
}


void
sf_client_sync_items_async (SfClient            *self,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_run_in_thread (task, sf_client_sync_items_worker);
}

gboolean
sf_client_sync_items_finish (SfClient      *self,
                             GAsyncResult  *res,
                             GError       **error)
{
  g_return_val_if_fail (SF_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (res), FALSE);

  return g_task_propagate_boolean (G_TASK (res), error);
}

/**
 * sf_client_get_items:
 * @self: a #SfClient
 *
 * Returns all synched items from the client
 *
 * Returns: (element-type SfItem) (transfer container): list with #SfItem objects
 *
 * Since: 0.2
 */
GPtrArray *
sf_client_get_items (SfClient *self)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);
  g_return_val_if_fail (SF_IS_CLIENT (self), NULL);

  return sf_provider_container (priv->provider);
}

void
sf_client_add_item (SfClient *self,
                    SfItem   *item)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_if_fail (SF_IS_CLIENT (self));
  g_return_if_fail (SF_IS_ITEM (item));

  sf_provider_add (priv->provider, item);
}

SfProvider *
sf_client_get_provider (SfClient *self)
{
  SfClientPrivate *priv = sf_client_get_instance_private (self);

  g_return_val_if_fail (SF_IS_CLIENT (self), NULL);

  return priv->provider;
}
