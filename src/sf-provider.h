#pragma once

#include <glib-object.h>
#include "sf-item.h"

G_BEGIN_DECLS

#define SF_TYPE_PROVIDER (sf_provider_get_type())

G_DECLARE_FINAL_TYPE (SfProvider, sf_provider, SF, PROVIDER, GObject)

SfProvider *sf_provider_new       (void);
gboolean    sf_provider_add       (SfProvider *self,
                                   SfItem     *item);
void        sf_provider_add_tag   (SfProvider *self,
                                   SfItem     *item);
void        sf_provider_remove    (SfProvider *self,
                                   SfItem     *item);
GPtrArray  *sf_provider_container (SfProvider *self);

G_END_DECLS
