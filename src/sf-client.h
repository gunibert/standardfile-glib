/*
 * sf-client.h
 *
 *  Created on: Mar 9, 2019
 *      Author: gunibert
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>
#include "sf-item.h"
#include "sf-provider.h"

G_BEGIN_DECLS

#define SF_TYPE_CLIENT (sf_client_get_type())

G_DECLARE_DERIVABLE_TYPE (SfClient, sf_client, SF, CLIENT, GObject)

struct _SfClientClass
{
  GObjectClass parent_class;
};

SfClient    *sf_client_new                 (const gchar          *host);
gboolean     sf_client_register            (SfClient             *self,
                                            const gchar          *email,
                                            const gchar          *password,
                                            GError              **error);
gboolean     sf_client_authenticate        (SfClient             *self,
                                            const char           *email,
                                            const char           *password,
                                            GError              **error);
void         sf_client_authenticate_async  (SfClient             *self,
                                            const gchar          *email,
                                            const gchar          *password,
                                            GCancellable         *cancellable,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);
gboolean     sf_client_authenticate_finish (SfClient             *self,
                                            GAsyncResult         *res,
                                            GError              **error);
void         sf_client_set_jwt             (SfClient             *self,
                                            const gchar          *jwt);
const gchar *sf_client_get_jwt             (SfClient             *self);
void         sf_client_set_hash            (SfClient             *self,
                                            const gchar          *hash);
const gchar *sf_client_get_hash            (SfClient             *self);
void         sf_client_sync_items          (SfClient             *self,
                                            GError              **error);
void         sf_client_sync_items_async    (SfClient             *self,
                                            GCancellable         *cancellable,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);
gboolean     sf_client_sync_items_finish   (SfClient             *self,
                                            GAsyncResult         *res,
                                            GError              **error);
GPtrArray   *sf_client_get_items           (SfClient             *self);
SfProvider  *sf_client_get_provider        (SfClient             *self);
void         sf_client_add_item            (SfClient             *self,
                                            SfItem               *item);

G_END_DECLS
