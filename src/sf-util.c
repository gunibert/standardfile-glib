#include "sf-util.h"
#include <stdio.h>
#include <assert.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/hmac.h>
#include <openssl/err.h>


static void
handleErrors (void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

/**
 * sf_util_encrypt:
 * @plain: the plaintext string
 * @key: the key
 * @iv: the initial vector
 * @cipher: (out): the ciphertext result
 *
 *
 * Returns: the length of the cipher
 *
 * Since: 0.2
 */
gint
sf_util_encrypt (const guchar  *plain,
                 const guchar  *key,
                 const guchar  *iv,
                 guchar       **cipher)
{
  EVP_CIPHER_CTX *ctx = NULL;
  gint plainlen = 0;
  gint clen = 0;
  gint cipherlen = 0;
  gint l = 0;

  plainlen = strlen((char *)plain);
  clen = plainlen + AES_BLOCK_SIZE;
  *cipher = g_malloc0 (clen);

  ctx = EVP_CIPHER_CTX_new ();
  EVP_EncryptInit (ctx, EVP_aes_256_cbc (), key, iv);

  EVP_EncryptUpdate (ctx, *cipher, &l, plain, plainlen);

  cipherlen += l;
  EVP_EncryptFinal (ctx, *cipher + cipherlen, &l);
  cipherlen += l;

  EVP_CIPHER_CTX_free (ctx);

  return cipherlen;
}

/**
 * sf_util_decrypt:
 * @ciphertext: the ciphertext
 * @ciphertext_len: the length of the ciphertext
 * @key: the key
 * @iv: the initial vector
 * @plaintext: the result of the decryption
 *
 *
 * Returns: the length of the plaintext
 *
 * Since: 0.2
 */
gint
sf_util_decrypt (guchar *ciphertext,
                 gint    ciphertext_len,
                 guchar *key,
                 guchar *iv,
                 guchar *plaintext)
{
  EVP_CIPHER_CTX *ctx = NULL;
  gint len = 0;
  gint plaintext_len = 0;

  /* Create and initialise the context */
  if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

  /* Initialise the decryption operation. IMPORTANT - ensure you use a key
   * and IV size appropriate for your cipher
   * In this example we are using 256 bit AES (i.e. a 256 bit key). The
   * IV size for *most* modes is the same as the block size. For AES this
   * is 128 bits */
  if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
    handleErrors();

  OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);

  /* Provide the message to be decrypted, and obtain the plaintext output.
   * EVP_DecryptUpdate can be called multiple times if necessary
   */
  if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handleErrors();
  plaintext_len = len;

  /* Finalise the decryption. Further plaintext bytes may be written at
   * this stage.
   */
  if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) handleErrors();
  plaintext_len += len;

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

/**
 * sf_util_bin_to_hex:
 * @bin: the binary string
 * @length: the length of the binary string
 *
 * Converts a binary string to hexadecimal
 *
 * Returns: (transfer full): hexadecimal representation of binary string
 *
 * Since: 0.2
 */
gchar *
sf_util_bin_to_hex (guchar *bin,
                    gint    length)
{
  gint i = 0;
  gchar *hexhash = NULL;

  hexhash = g_malloc0 (length * 2 + 1);

  for(i = 0; i < length; i++)
    {
      sprintf(hexhash + (i * 2), "%02x", bin[i]);
    }

  hexhash[i*2] = '\0';
  return hexhash;
}


static gchar
mapping_hex (gchar hex)
{
  switch (hex)
    {
    case '0':
      return 0x0;
    case '1':
      return 0x1;
    case '2':
      return 0x2;
    case '3':
      return 0x3;
    case '4':
      return 0x4;
    case '5':
      return 0x5;
    case '6':
      return 0x6;
    case '7':
      return 0x7;
    case '8':
      return 0x8;
    case '9':
      return 0x9;
    case 'a':
      return 0xA;
    case 'b':
      return 0xB;
    case 'c':
      return 0xC;
    case 'd':
      return 0xD;
    case 'e':
      return 0xE;
    case 'f':
      return 0xF;
    default:
      return '\0';
    }
}

/*
 * f == 1111
 * ff == 11111111 == 8 bit == 1 byte == 1 char
 */
/**
 * sf_util_hex_to_bin:
 * @hex: the hex string
 *
 * Convert hexadecimal string to binary
 *
 * Returns: binary representation of hex string
 *
 * Since: 0.2
 */
guchar *
sf_util_hex_to_bin (const gchar *hex)
{
  guchar *bin = NULL;
  int len = 0;

  len = strlen (hex);
  bin = g_malloc0 (len / 2);

  for (gint i = 0; i < len; i+=2)
    {
      gchar p1 = mapping_hex (hex[i]);
      gchar p2 = mapping_hex (hex[i+1]);
      gchar p = (p1 << 4) + p2;
      bin[i/2] = p;
    }

  return bin;
}


/**
 * sf_util_substring:
 * @input: the source string
 * @offset: the start of the substring
 * @len: the length of the substring starting at #offset
 *
 * Extract the substring between offset and offset + len.
 *
 * Returns: a substring. Free after usage.
 *
 * Since: 0.2
 */
gchar *
sf_util_substring (const gchar *input,
                   guint        offset,
                   guint        len)
{
  gchar *dest = NULL;
  guint32 input_len = 0;

  if (input == NULL) return NULL;

  input_len = strlen (input);
  if (offset + len > input_len || offset > input_len || len > input_len)
    return NULL;

  dest = g_strndup (input + offset, len);
  return dest;
}

/**
 * sf_util_generate_random_bits:
 * @bits: the size of the string
 *
 * Generates a string with random bits. Converts the string to hexadecimal
 * format. Free when done with it.
 *
 * Returns: a string with random bits
 *
 * Since: 0.2
 */
gchar *
sf_util_generate_random_bits (gint bits)
{
  gint64 chunk = 0;
  guchar *r = NULL;

  chunk = bits / 8;
  r = malloc (sizeof (guchar) * chunk);

  for (gint i = 0; i < chunk; i++)
    {
      r[i] = rand () % CHAR_MAX;
    }

  return sf_util_bin_to_hex ((guchar *)r, chunk);
}

/**
 * sf_util_generate_random_string:
 * @length: the length of the string
 *
 * Generates a random string. The string should be freed when done.
 *
 * Returns: a random string with #length length
 *
 * Since: 0.2
 */
gchar *
sf_util_generate_random_string (gsize length)
{
  GRand *grand = NULL;
  gchar *random_string = NULL;
  gchar charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  grand = g_rand_new ();

  random_string = g_malloc_n (sizeof (gchar), length + 1);

  for (gsize i = 0; i < length; i++)
    {
      gint key = g_rand_int_range (grand, 0, sizeof (charset) - 1);
      random_string[i] = charset[key];
    }

  random_string[length] = '\0';
  return random_string;
}

/* void */
/* bin_to_hex_2 (guchar *bin, */
/*               gint    length, */
/*               gchar  *hex) */
/* { */
/*   gint i = 0; */
/*   for(; i < length; i++) */
/*     { */
/*       sprintf(hex + (i * 2), "%02x", bin[i]); */
/*     } */

/*   hex[i*2] = 0; */
/* } */
/*  * f == 1111 */
/*  * ff == 11111111 == 8 bit == 1 byte == 1 char */
/*  */
/* void */
/* hex_to_bin_2 (gchar  *hex, */
/*               guchar *hex_bin) */
/* { */
/*   int len = 0; */

/*   len = strlen (hex); */

/*   for (gint i = 0; i < len; i+=2) */
/*     { */
/*       gchar p1 = mapping_hex (hex[i]); */
/*       gchar p2 = mapping_hex (hex[i+1]); */
/*       gchar p = (p1 << 4) + p2; */
/*       hex_bin[i/2] = p; */
/*     } */
/* } */
