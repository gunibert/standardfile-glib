/* sf-item.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "sf-content.h"

G_BEGIN_DECLS

#define SF_TYPE_ITEM (sf_item_get_type())

G_DECLARE_DERIVABLE_TYPE (SfItem, sf_item, SF, ITEM, GObject)

struct _SfItemClass
{
  GObjectClass parent_class;
};

SfItem      *sf_item_new               (void);
SfItem      *sf_item_new_with_uuid     (const gchar *uuid);
gboolean     sf_item_is_note           (SfItem      *self);
const gchar *sf_item_get_uuid          (SfItem      *self);
const gchar *sf_item_get_title         (SfItem      *self);
void         sf_item_set_title         (SfItem      *self,
                                        const gchar *title);
const gchar *sf_item_get_text          (SfItem      *self);
void         sf_item_set_text          (SfItem      *self,
                                        const gchar *text);
const gchar *sf_item_get_preview       (SfItem      *self);
const gchar *sf_item_get_content_type  (SfItem      *self);
void         sf_item_set_updated_at    (SfItem      *self,
                                        GDateTime   *updated_at);
gchar       *sf_item_get_updated_at    (SfItem      *self);
GDateTime   *sf_item_get_updated_at_dt (SfItem      *self);
void         sf_item_set_created_at    (SfItem      *self,
                                        GDateTime   *created_at);
gchar       *sf_item_get_created_at    (SfItem      *self);
void         sf_item_set_dirty         (SfItem      *self,
                                        gboolean     dirty);
gboolean     sf_item_is_dirty          (SfItem      *self);
void         sf_item_delete            (SfItem      *self,
                                        gboolean     deleted);
gboolean     sf_item_is_deleted        (SfItem      *self);
SfContent   *_sf_item_get_content      (SfItem      *self);

G_END_DECLS
