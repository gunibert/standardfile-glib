/* sf-codec.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "sf-codec"

#include "sf-codec.h"
#include <openssl/evp.h>
#include "sflib.h"

struct _SfCodec
{
  GObject parent_instance;
};

G_DEFINE_TYPE (SfCodec, sf_codec, G_TYPE_OBJECT)

SfCodec *
sf_codec_new (void)
{
  return g_object_new (SF_TYPE_CODEC, NULL);
}

static void
sf_codec_finalize (GObject *object)
{
  G_OBJECT_CLASS (sf_codec_parent_class)->finalize (object);
}

static void
sf_codec_class_init (SfCodecClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sf_codec_finalize;
}

static void
sf_codec_init (_unused SfCodec *self)
{
}

/*
 * for now i parse the json nodes like the item did before. This should be
 * refactored because i don't want this logic be part of a client
 */

static void
sf_codec_decrypt_check_local_auth_hash (const char *auth_hash,
                                        const char *auth_key,
                                        const char *version,
                                        const char *uuid,
                                        const char *iv,
                                        const char *ciphertext)
{
  g_autofree gchar  *content = NULL;
  g_autofree guchar *auth_key_bin = NULL;
  g_autofree gchar  *local_auth_hash = NULL;

  content = g_strjoin (":", version, uuid, iv, ciphertext, NULL);
  auth_key_bin = sf_util_hex_to_bin (auth_key);

  local_auth_hash = g_compute_hmac_for_string (G_CHECKSUM_SHA256,
                                               auth_key_bin,
                                               strlen (auth_key)/2,
                                               content,
                                               -1);

  // FIXME: gracefully shutdown
  if (g_strcmp0 (local_auth_hash, auth_hash) != 0)
    g_error ("This indicates tampering or a malfunction in the server");
}


static char *
sf_codec_decrypt_string_003 (const char *enc_item_key,
                             char       *enc_key,
                             char       *auth_key,
                             const char *uuid)
{
  g_autofree guchar *iv_bin = NULL;
  g_autofree guchar *plaintext = NULL;
  g_autofree guchar *encryption_key_bin = NULL;
  g_autofree guchar *b64ciphertext = NULL;
  gchar **components = NULL;
  gchar *version = NULL;
  gchar *auth_hash = NULL;
  gchar *local_uuid = NULL;
  gchar *iv = NULL;
  gchar *ciphertext = NULL;
  gsize b64cipherlen = 0;
  gsize plainlen = 0;

  components = g_strsplit (enc_item_key, ":", -1);

  version    = components[0];
  auth_hash  = components[1];
  local_uuid = components[2];
  iv         = components[3];
  ciphertext = components[4];

  plaintext = g_malloc0 ((strlen (ciphertext) + 1) * sizeof (gchar));

  iv_bin = sf_util_hex_to_bin (iv);
  SF_TRACE ("ciphertext: %s", ciphertext);

  if (g_strcmp0 (local_uuid, uuid) != 0) {
    g_error ("uuid mismatch. This indicates tampering or a malfunction in the server.\nGot: %s, Expected: %s", local_uuid, uuid);
  }

  sf_codec_decrypt_check_local_auth_hash (auth_hash, auth_key, version, uuid, iv, ciphertext);

  encryption_key_bin = sf_util_hex_to_bin (enc_key);

  b64ciphertext = g_base64_decode (ciphertext, &b64cipherlen);
  plainlen = sf_util_decrypt (b64ciphertext, b64cipherlen, encryption_key_bin, iv_bin, plaintext);
  plaintext[plainlen] = '\0';

  g_strfreev (components);
  return g_strdup ((char *)plaintext);
}

static void
sf_codec_parse_content (_unused SfCodec *self,
                        SfItem  *item,
                        char    *json)
{
  g_autoptr(JsonParser) parser = NULL;
  JsonNode *root = NULL;
  JsonObject *obj = NULL;
  GError *error = NULL;
  const gchar *title = NULL;
  const gchar *text = NULL;

  parser = json_parser_new ();
  if (json_parser_load_from_data (parser, json, -1, &error) == FALSE)
    {
      // do something with error
      g_error ("%s: %s\n", "was not able to parse content", error->message);
      g_clear_error (&error);
      return;
    }

  root = json_parser_get_root (parser);
  obj = json_node_get_object (root);

  if (json_object_has_member (obj, "title"))
    title = json_object_get_string_member (obj, "title");
  if (json_object_has_member (obj, "text"))
    text  = json_object_get_string_member (obj, "text");

  sf_item_set_title (item, title != NULL?title:"");
  sf_item_set_text (item, text != NULL?text:"");
}

/**
 * sf_codec_decrypt:
 * @self: a #SfCodec
 * @root: the root json node for that item
 * @authentication: authentication informations used for decryption
 *
 * Decrypts a Json object and creates a new #SfItem
 *
 * Returns: (transfer full): a #SfItem
 *
 * Since: 0.2
 */
SfItem *
sf_codec_decrypt (SfCodec    *self,
                  JsonNode   *root,
                  GHashTable *authentication)
{
  g_autofree const gchar *version = NULL;
  JsonObject *item_obj = NULL;
  GDateTime *updated_at = NULL;
  GDateTime *created_at = NULL;
  const gchar *content_type = NULL;
  const gchar *uuid = NULL;
  const gchar *content = NULL;
  const gchar *enc_item_key = NULL;
  const gchar *updated_at_str = NULL;
  const gchar *created_at_str = NULL;
  gboolean deleted = FALSE;

  item_obj = json_node_get_object (root);

  content_type = json_object_get_string_member (item_obj, "content_type");
  uuid = json_object_get_string_member (item_obj, "uuid");
  content = json_object_get_string_member (item_obj, "content");
  enc_item_key = json_object_get_string_member (item_obj, "enc_item_key");
  updated_at_str = json_object_get_string_member (item_obj, "updated_at");
  updated_at = g_date_time_new_from_iso8601 (updated_at_str, NULL);
  created_at_str = json_object_get_string_member (item_obj, "created_at");
  created_at = g_date_time_new_from_iso8601 (created_at_str, NULL);
  deleted = json_object_get_boolean_member (item_obj, "deleted");
  version = g_strndup (enc_item_key, 3);

  if (g_strcmp0 (version, "001") == 0 ||
      g_strcmp0 (version, "002") == 0)
    {
      g_error ("Old encryption protocol detected. This is not support by this library. "
               "Please resync all of your notes according to standardnotes.org/help/resync");
    }
  else if (g_strcmp0 (version, "003") == 0)
    {
      g_autofree gchar *item_key = NULL;
      g_autofree gchar *item_ek = NULL;
      g_autofree gchar *item_ak = NULL;
      SfItem *item = NULL;
      gchar *content_decrypt = NULL;

      item_key = sf_codec_decrypt_string_003 (enc_item_key,
                                              g_hash_table_lookup (authentication, "mk"),
                                              g_hash_table_lookup (authentication, "ak"),
                                              uuid);
      item_ek = sf_util_substring (item_key, 0, 64);
      item_ak = sf_util_substring (item_key, 64, 64);
      content_decrypt = sf_codec_decrypt_string_003 (content, item_ek, item_ak, uuid);
      item = sf_item_new_with_uuid (uuid);
      sf_codec_parse_content (self, item, content_decrypt);
      sf_item_set_updated_at (item, updated_at);
      sf_item_set_created_at (item, created_at);
      sf_item_delete (item, deleted);
      // FIXME make this nicer

      g_object_set (item, "content-type", content_type, NULL);
      g_free (content_decrypt);
      return item;
    }
  else
    {
      SfItem *item = NULL;
      item = sf_item_new_with_uuid (uuid);

      sf_item_set_updated_at (item, updated_at);
      sf_item_set_created_at (item, created_at);
      sf_item_delete (item, deleted);
      g_object_set (item, "content-type", content_type, NULL);
      return item;
    }
  return NULL;
}

static char *
sf_codec_encrypt_string_003 (_unused SfCodec    *self,
                             char       *string_to_encrypt,
                             char       *enc_key,
                             char       *auth_key,
                             const char *uuid)
{
  g_autofree guchar *enc_key_bin = NULL;
  g_autofree guchar *iv_bin = NULL;
  g_autofree guchar *ciphertext = NULL;
  g_autofree guchar *auth_key_bin = NULL;
  g_autofree gchar *ctext = NULL;
  g_autofree gchar *string_to_auth = NULL;
  g_autofree gchar *content_result = NULL;
  gchar *iv = NULL;
  gsize ciphertext_len = 0;

  g_return_val_if_fail (string_to_encrypt != NULL, NULL);

  iv = sf_util_generate_random_bits (128);
  enc_key_bin = sf_util_hex_to_bin (enc_key);
  iv_bin = sf_util_hex_to_bin (iv);

  SF_TRACE ("enc_key: %s", enc_key);
  SF_TRACE ("auth_key: %s", auth_key);
  SF_TRACE ("IV: %s", iv);
  ciphertext_len = sf_util_encrypt ((guchar *) string_to_encrypt,
                                    enc_key_bin,
                                    iv_bin,
                                    &ciphertext);

  ctext = g_base64_encode (ciphertext, ciphertext_len);

  string_to_auth = g_strjoin (":", "003", uuid, iv, ctext, NULL);

  auth_key_bin = sf_util_hex_to_bin (auth_key);
  content_result = g_compute_hmac_for_string (G_CHECKSUM_SHA256,
                                              auth_key_bin,
                                              strlen (auth_key)/2,
                                              string_to_auth,
                                              -1);

  return g_strjoin (":", "003", content_result, uuid, iv, ctext, NULL);
}

JsonNode *
sf_codec_encrypt (SfCodec    *self,
                  SfItem     *item,
                  GHashTable *authentication)
{
  g_autoptr(JsonBuilder) builder = NULL;
  g_autofree gchar *item_ek = NULL;
  g_autofree gchar *item_ak = NULL;
  g_autofree gchar *string_to_encrypt = NULL;
  g_autofree gchar *content = NULL;
  g_autofree gchar *enc_item_key = NULL;
  g_autofree gchar *updated_at = NULL;
  g_autofree gchar *created_at = NULL;
  const gchar *uuid = NULL;
  gchar *item_key = NULL;

  uuid = sf_item_get_uuid (item);
  builder = json_builder_new ();

  item_key = sf_util_generate_random_bits (512);

  item_ek = sf_util_substring (item_key, 0, 64);
  item_ak = sf_util_substring (item_key, 64, 64);

  string_to_encrypt = json_gobject_to_data (G_OBJECT (_sf_item_get_content (item)), NULL);

  content = sf_codec_encrypt_string_003 (self, string_to_encrypt, item_ek, item_ak, uuid);
  enc_item_key = sf_codec_encrypt_string_003 (self,
                                              item_key,
                                              g_hash_table_lookup (authentication, "mk"),
                                              g_hash_table_lookup (authentication, "ak"),
                                              uuid);

  builder = json_builder_begin_object (builder);

  builder = json_builder_set_member_name (builder, "uuid");
  builder = json_builder_add_string_value (builder, sf_item_get_uuid (item));

  builder = json_builder_set_member_name (builder, "content_type");
  builder = json_builder_add_string_value (builder, sf_item_get_content_type (item));

  builder = json_builder_set_member_name (builder, "enc_item_key");
  builder = json_builder_add_string_value (builder, enc_item_key);

  builder = json_builder_set_member_name (builder, "content");
  builder = json_builder_add_string_value (builder, content);

  builder = json_builder_set_member_name (builder, "deleted");
  builder = json_builder_add_boolean_value (builder, sf_item_is_deleted (item));

  builder = json_builder_set_member_name (builder, "updated_at");
  updated_at = sf_item_get_updated_at (item);
  builder = json_builder_add_string_value (builder, updated_at);

  builder = json_builder_set_member_name (builder, "created_at");
  created_at = sf_item_get_created_at (item);
  builder = json_builder_add_string_value (builder, created_at);

  builder = json_builder_end_object (builder);

  return json_builder_get_root (builder);
}
